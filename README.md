## Telepítés ##

### For Symfony >= 2.3.* ###



### Az alábbi modulok kell telepíteni ###

Találati lista szűkítéshez:
```
lexik/form-filter-bundle
```
Találati lista exportálása:
```
ee/dataexporter-bundle
```
Találati lista lapozása:
```
knplabs/knp-paginator-bundle
```
Opcionális:

Üzenet létrehozása időpontjának mentése, illetve a Softdeletable miatt:
```
knplabs/doctrine-behaviors
```

Fel kell venni a composer.json fájlba:

```
{
    "require": {
        "makai/contact-bundle": "dev-master",
    },
     "repositories": [
        {
            "type": "vcs",
            "url": "https://lajos_makai@bitbucket.org/lajos_makai/contactbundle.git"
        }
    ]
}
```


### Bundle telepítése ###

```
$ php composer.phar update makai/contact-bundle
```

### Bundle regisztrálása###

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        new Makai\ContactBundle\MakaiContactBundle(),
    );
}
```

app/routing.yml fájlba fel kell venni:

```
# MakaiContactBundle
makai_contact:
    resource: "@MakaiContactBundle/Resources/config/routing.yml"
    prefix:   /
```

### Elvégzendő módosítások ###
Saját ContactBundle-t kell generálni:
```
$ php app/console generate:bundle
```
Ezután létre kell hozni egy ContactEntry entitást azt alábbi tartalommal.
A namespace és a User entitás megváltoztatása szüséges a projectnek megfelelőre.
```
<?php

namespace Europestream\ContactBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Makai\ContactBundle\Entity\ContactEntry as BaseContact;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * ContactEntry
 * @ORM\Table(name="contact_entry", indexes={
 *      @ORM\Index(name="deletedAt_idx", columns={"deletedAt"}),
 *      @ORM\Index(name="createdAt_idx", columns={"createdAt"}),
 *      @ORM\Index(name="updatedAt_idx", columns={"updatedAt"})
 * })
 * @ORM\Entity
 */
class ContactEntry extends BaseContact
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

    /**
     * @var integer
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var \Europestream\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\Europestream\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Set cUser
     *
     * @param \Europestream\UserBundle\Entity\User $user
     * @return ContactEntry
     */
    public function setUser($user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Europestream\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
```


### Bundle paraméterezési lehetőségei ###
```
# milyen cimre menjen ertesito email a kapcsolati üzenetről
contactbundle.emails.contact_email: info@makai.hu
# milyen cimről menjen ertesito email a kapcsolati üzenetről
contactbundle.emails.from_email: info@makai.hu
#csak regisztrált felhasználók küldhetnek üzenetet?
contactbundle.allow.only_registered: false
#a felhasználókat kezelő entitás pl.:\Europestream\ContactBundle\Entity\ContactEntry
contactbundle_user_class:
```



### Adatbázis schema frissítése ###
```
php app/console doctrine:schema:update --force
```
