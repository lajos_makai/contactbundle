<?php
/*
* This file is part of the MakaiContactBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\ContactBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MakaiContactBundle extends Bundle
{
}
