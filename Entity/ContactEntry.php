<?php
/*
* This file is part of the MakaiContactBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\ContactBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Makai\ContactBundle\Model\ContactInterface;

/**
 * @ORM\MappedSuperclass
 */
class ContactEntry implements ContactInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="text", nullable=false)
     */
    private $user_name;

    /**
     * @var string
     *
     * @ORM\Column(name="user_email", type="text", nullable=false)
     */
    private $user_email;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text", nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=false)
     */
    private $message;

    /**
     * Set email
     *
     * @param string $user_email
     * @return ContactEntry
     */
    public function setUserEmail($user_email)
    {
        $this->user_email = $user_email;

        return $this;
    }

    /**
     * Get user_email
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->user_email;
    }

    /**
     * Set user_name
     *
     * @param string $user_name
     * @return ContactEntry
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;

        return $this;
    }

    /**
     * Get user_name
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }
    /**
     * Set subject
     *
     * @param string $subject
     * @return ContactEntry
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return ContactEntry
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('user_name', new NotBlank(array(
            'message' => 'contact.name.not_blank',
        )));
        $metadata->addPropertyConstraint('user_email', new NotBlank(array(
            'message' => 'contact.email.not_blank',
        )));
        $metadata->addPropertyConstraint('message', new NotBlank(array(
            'message' => 'contact.message.not_blank',
        )));
        $metadata->addPropertyConstraint('subject', new NotBlank(array(
            'message' => 'contact.subject.not_blank',
        )));
    }
}