<?php
/*
* This file is part of the MakaiContactBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\ContactBundle\Model;

interface ContactInterface
{

    public function setUserEmail($user_email);

    public function getUserEmail();

    public function setUserName($user_name);

    public function getUserName();

    public function setSubject($subject);

    public function getSubject();

    public function setMessage($message);

    public function getMessage();
}