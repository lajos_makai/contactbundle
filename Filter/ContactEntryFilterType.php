<?php
/*
* This file is part of the MakaiContactBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\ContactBundle\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Extension\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;

class ContactEntryFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('subject', 'filter_text', array(
                    'label' => 'form.filter.contact.subject',
			        'condition_pattern' => FilterOperands::STRING_BOTH)
        );
        $builder->add('username', 'filter_text', array(
                'label' => 'form.filter.contact.username',
                'condition_pattern' => FilterOperands::STRING_BOTH)
        );
        $builder->add('useremail', 'filter_text', array(
                'label' => 'form.filter.contact.useremail',
                'condition_pattern' => FilterOperands::STRING_BOTH)
        );
    }

    public function getName()
    {
        return 'contact_filter';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}