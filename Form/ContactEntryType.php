<?php
/*
* This file is part of the MakaiContactBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\ContactBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactEntryType extends AbstractType
{

    private $class;

    /**
     * @param string $class The ContactEntity class name
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_name', 'text', array(
                    'label' => 'contact.form.field.username'
                )
            )
            ->add('user_email', 'text', array(
                    'label' => 'contact.form.field.email'
                )
            )
            ->add('subject','text',array(
                    'label' => 'contact.form.field.subject'
                )
            )
            ->add('message', 'textarea',array(
                    'label' => 'contact.form.field.body'
                )
            )

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class
        ));
    }

    public function getName()
    {
        return 'contact';
    }
}
