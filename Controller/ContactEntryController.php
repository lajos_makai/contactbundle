<?php
/*
* This file is part of the MakaiContactBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\ContactBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Makai\ContactBundle\Entity\ContactEntry;
use Makai\ContactBundle\Form\ContactEntryType;
use Makai\ContactBundle\Filter\ContactEntryFilterType;

/**
 * ContactEntry controller.
 *
 */
class ContactEntryController extends Controller
{

    /**
     * Lists all ContactEntry entities.
     *
     */

    public function indexAction()
    {

        $form = $this->get('form.factory')->create(new ContactEntryFilterType());

        $filterBuilder = $this->get('doctrine.orm.entity_manager')
            ->getRepository($this->container->getParameter('contactbundle_user_class'))
            ->createQueryBuilder('ce')
            ->orderBy('ce.createdAt', 'DESC');

        if ($this->get('request')->query->has('submit-filter')) {
            $form->bind($this->get('request'));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }

        $entities = $filterBuilder->getQuery();
 
        if ($this->get('request')->query->has('export') && $this->get('request')->query->get('export') ==  1) {
            $exportData = $entities->getResult();
            $exporter = $this->get('ee.dataexporter');
            $exporter->setOptions($this->get('request')->query->get('export_format'), array('fileName' => $this->get('translator')->trans('contact.export.filename')));
            $exporter->setColumns(array(
                'subject' => $this->get('translator')->trans('contact.export.field.subject'),
                'message' => $this->get('translator')->trans('contact.export.field.message'),
                'user_name' => $this->get('translator')->trans('contact.export.field.user_name'),
                'user_email' => $this->get('translator')->trans('contact.export.field.user_email'),
                'createdAt' => $this->get('translator')->trans('contact.export.field.createdAt')
            ));
            $exporter->addHook(function($createdAt){return $createdAt->format('Y-m-d H:i:s');}, 'createdAt');
            $exporter->setData($exportData);

            return $exporter->render();
        }

        $filterBuilder->select(' count(ce.id)');
        $query  = $filterBuilder->getQuery();
        $allOrderCount = $query->getSingleScalarResult();


        $entities->setHint('knp_paginator.count', $allOrderCount);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            $this->container->getParameter('paginator.default.limit')
        );

        return $this->render('MakaiContactBundle:ContactEntry:index.html.twig', array(
            "pagination"=>$pagination,
            "form" => $form->createView()
        ));
    }
    /**
     * Creates a new ContactEntry entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new ContactEntry();
        $form = $this->createForm(new ContactEntryType(), $entity);
        $form->handleRequest($request);;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            /*sikeres mentés jelzése a felhasználónak*/
            $this->get('session')
                ->getFlashBag()
                ->add('contact-success', $this->get('translator')->trans('save.success'));

            return $this->redirect($this->generateUrl('admin_contact', array('id' => $entity->getId())));
        }

        return $this->render('MakaiContactBundle:ContactEntry:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new ContactEntry entity.
     *
     */
    public function newAction()
    {
        $entity = new ContactEntry();
        $form   = $this->createForm(new ContactEntryType($this->container->getParameter('contactbundle_user_class')), $entity);

        return $this->render('MakaiContactBundle:ContactEntry:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ContactEntry entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($this->container->getParameter('contactbundle_user_class'))->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactEntry entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MakaiContactBundle:ContactEntry:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing ContactEntry entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($this->container->getParameter('contactbundle_user_class'))->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactEntry entity.');
        }

        $editForm = $this->createForm(new ContactEntryType($this->container->getParameter('contactbundle_user_class')), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MakaiContactBundle:ContactEntry:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing ContactEntry entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($this->container->getParameter('contactbundle_user_class'))->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactEntry entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ContactEntryType($this->container->getParameter('contactbundle_user_class')), $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            /*sikeres mentés jelzése a felhasználónak*/
            $this->get('session')
                ->getFlashBag()
                ->add('contact-success', $this->get('translator')->trans('save.success'));

            return $this->redirect($this->generateUrl('admin_contact_edit', array('id' => $id)));
        }
        var_dump($editForm->getErrors());
        return $this->render('MakaiContactBundle:ContactEntry:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ContactEntry entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($this->container->getParameter('contactbundle_user_class'))->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ContactEntry entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_contact'));
    }

    /**
     * Creates a form to delete a ContactEntry entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
