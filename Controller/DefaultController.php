<?php
/*
* This file is part of the MakaiContactBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\ContactBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Makai\ContactBundle\Entity\ContactEntry;
use Makai\ContactBundle\Form\ContactEntryType;

class DefaultController extends Controller
{
	/**
	 * Kapcsolat oldal, email küldése ez adminnak a kapcsalati form-on megadott adatokkal
	 */
	public function contactAction()
    {
        $userClass = $this->container->getParameter('contactbundle_user_class');

        $contactEntity = new $userClass;

    	//meg kell vizsgálni, hogy csak belépett user küldhet-e üzenetet
    	if($this->container->getParameter('contactbundle_allow_only_registered') === "true"){
	    	/*csak belépett felhasználó küldhet üzenetet*/
	    	if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
	    		return $this->redirect($this->generateUrl('fos_user_security_login'));
	    	}
    	}

        /*a belépett felhasználóknak feltöltjük előre adatokkal*/
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {

            $user = $this->container->get('security.context')->getToken()->getUser();

            $contactEntity->setUserEmail($user->getEmail());
            $contactEntity->setUserName($user->getFullName());
            $contactEntity->setUser($user);
        }

    	$form = $this->createForm(new ContactEntryType($this->container->getParameter('contactbundle_user_class')), $contactEntity);

        $request = $this->getRequest();
        
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
    
            if ($form->isValid()) {

            	$em = $this->getDoctrine()->getManager();
            	$em->persist($contactEntity);
            	$em->flush();

                /*sikeres mentés jelzése a felhasználónak*/
                $this->get('session')
                    ->getFlashBag()
                    ->add('contact-notice', $this->get('translator')->trans('contact.message.success'));

            	// ha minden mező helyesen volt kitöltve, akkor elküldjük az e-mailt
                $message = \Swift_Message::newInstance()
                    ->setSubject($this->get('translator')->trans('contact.message.subject'))
                    ->setFrom($this->container->getParameter('contactbundle_emails_from_email'))
                    ->setTo($this->container->getParameter('contactbundle_emails_contact_email'))
                    ->setBody($this->renderView('MakaiContactBundle:Default:contactEmail.txt.twig', array('enquiry' => $contactEntity)));
                $this->get('mailer')->send($message);
                
                /*sikeres mail küldés után átírányítjuk a kapcsolat oldalra, hogy ne ha frissíti az oldalt, akkor
                 * ne küldje el újra az üzenetet
                 */
                //return $this->redirect($this->generateUrl('contact'));
            }
        }
    
        return $this->render('MakaiContactBundle:Default:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
